train_accs = [];
test_accs = [];

for k=1:5
  train_data = [];
  train_labels = [];
  for i=1:5
    load(sprintf('subset_CIFAR10/small_data_batch_%u.mat', i));
    if i == k
      test_data = data;
      test_labels = labels;
    else
      train_data = cat(dim=1, train_data, data);
      train_labels = cat(dim=1, train_labels, labels);
    endif
  endfor
  train2(train_data, train_labels);
  load('Model2.mat');
  y = classify2(Model2, train_data);  
  train_accs(end+1) = sum(y == train_labels) / length(y);
  y = classify2(Model2, test_data);
  test_accs(end+1) = sum(y == test_labels) / length(y);
  
endfor

train_accs
test_accs
mean(train_accs)
mean(test_accs)
