function [ids, centers] = kmeans(data, k=10)
  data = cast(data, 'double');
  centers = rand(k, columns(data));
  newids = zeros(rows(data), 1);

  do

  ids = newids;
  newids = zeros(rows(data), 1);


  for i=1:rows(data)
    dists = norm(centers - data(i, :), 'rows');
    newids(i, 1) = find(dists == min(dists));
  endfor

  for i=1:k
    centers(i, :) = sum((newids == i) .* data, dim=1) / max(1, sum(newids == i));
  endfor

  until (newids == ids)

endfunction