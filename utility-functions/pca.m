function [pca_vecs]=pca(data, variance_threshold=1.0)
  ## get eigenvectors and eigenvalues for covariance matrix
  [v, lambda] = eig(balance(cov(center(data))));

  ## sort both by size of eigenvalues
  [lambda, ordering] = sort(diag(lambda), mode='descend');
  v = v(:, ordering);

  ## select the number of eigenvectors required to account
  ## for at least a fraction variance_threshold of the data
  variance_fractions = cumsum(lambda) / sum(lambda);
  cutoff = sum(variance_fractions < variance_threshold);

  ## each column of pca_vecs is one of the principal components
  pca_vecs = v(:, 1:cutoff);

endfunction