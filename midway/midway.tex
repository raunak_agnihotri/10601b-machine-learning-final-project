\documentclass{article} % For LaTeX2e
\usepackage{nips13submit_e,times}
\usepackage{hyperref}
\usepackage{url}
\usepackage{amssymb}
\usepackage{ dsfont }
\usepackage{amsmath}

%\documentstyle[nips13submit_09,times,art10]{article} % For LaTeX 2.09


\title{10601B Project Midway Report}


\author{
Raunak Agnihotri \\
Carnegie Mellon University \\
5000 Forbes Avenue, Pittsburgh PA 15213 \\
\texttt{raunak@cmu.edu} \\
\And
Jonathan Humberson \\
Carnegie Mellon University \\
5000 Forbes Avenue, Pittsburgh PA 15213 \\
\texttt{jhumbers@andrew.cmu.edu} \\
}
% \AND
% Coauthor \\
% Affiliation \\
% Address \\
% \texttt{email} \\
% \And
% Coauthor \\
% Affiliation \\
% Address \\
% \texttt{email} \\
% \And
% Coauthor \\
% Affiliation \\
% Address \\
% \texttt{email} \\
% (if needed)\\


% The \author macro works with any number of authors. There are two commands
% used to separate the names and addresses of multiple authors: \And and \AND.
%
% Using \And between authors leaves it to \LaTeX{} to determine where to break
% the lines. Using \AND forces a linebreak at that point. So, if \LaTeX{}
% puts 3 of 4 authors names on the first line, and the last on the second
% line, try using \AND instead of \And before the third author name.

\newcommand{\fix}{\marginpar{FIX}}
\newcommand{\new}{\marginpar{NEW}}

\nipsfinalcopy % Uncomment for camera-ready version

\begin{document}


\maketitle

\begin{abstract}
The objective of this project is to investigate various Machine Learning techniques
and their effectiveness in classifying images from the CIFAR-10 dataset. This midway
report consists of a Naive Bayes classifier, coupled with PCA dimensionality reduction.
The classifier acheived a classification accuracy of 48\%.
\end{abstract}

\section{Introduction}
The CIFAR-10 dataset consists of 60000 32x32 colour ``tiny images'' in 10 classes, 
with 6000 images per class. There are 50000 training images and 10000 test images. 
For the purposes of this project, training was done on a subset of training images, 
consisting of 5000 images. 

\subsection{Motivation}
With the vast amount of data that is collected for the purpose of analysis to 
construct useful predictive models, there is a growing need for machine classifiers
that can achieve high levels of classification accuracy. This investigation will
consist of a comparative analysis of three classifiers (this midway report will only
contain the implementation, results and other details of one classifier), and its aim
is to gain some insight into effective methods of image classification. 

\subsection{Background and Related Work}
This midway report contains results of a Gaussian Naive Bayes Classifier.
One particularly important aspect of classification projects of this nature is the 
problem of feature extraction. The unprocessed RGB data is not necessarily a good
basis for accurate classification; thus, the classifier makes use of the histogram
of oriented gradients (HOG) features.

\section{Method}
% Brief overview of what HOG is. maybe some computer vision papers for reference.
\subsection{Training}
For each training example, the HOG features with a cell size of 8 are extracted. This
results in a transformed feature vector with 1984 components. In order to improve
the efficiency of the algorithm, principal component analysis is performed on the
transformed feature vector.

Define the following symbols:
$m$ - number of components in transformed features
$k$ - number of classes 
$f(\mu,\sigma^2)$ - density of the normal distribution with mean $\mu$ 
and variance $\sigma^2$

Outline of the Naive Bayes Classifier:

First, the prior probabilities are are computed.
\[ \mathbb{P}[Y = j] = \frac{\sum\limits_i \mathds{1}(Y^{(i)} = j)}{|Y|} \]

Estimate the mean and variance parameters:

\[\mu_{ij} = \overline{X_i|Y=j} \]
\[\sigma_{ij}^2 = \text{Var}[X_i|Y=j] \]

Then for each class, the conditional probability is estimated:
\[ \mathbb{P}[Y = j|X] = \frac{\mathbb{P}[Y = j] 
\prod\limits_i f(\mu_{ij},\sigma_{ij}^2)} 
{\sum\limits_l \mathbb{P}[Y=l]\prod\limits_i f(\mu_{il},\sigma_{il}^2)}\]


\subsection{Testing}
Testing is performed on a set of 1000 images.

\section{Experiments and Results}
The first hurdle to the accurate clasification of images is the extraction of vectorized features from those images. This is particularly important when the images in
 question are as small or low-resolution as the ones we are using here, because the generation of useful features is much more challenging. Thus, the majority of the work
 thus far has focused on the problem of feature extraction and processing, so as to make the task of classification as easy as possible for the classifiers we implement.
 To this end, we use a simple Gaussian naive Bayes (GNB) classifier to allow for easy comparison of various methods of feature extraction.

The most straightforward, albeit crude, method of feature extraction is to simply use the raw intensity values for each color channel as inputs to a classifier. Despite
 its crudity, this may still offer some ability to correctly classify certain types of images, e.g. images of airplanes on predominantly blue backgrounds. More
 importantly, it will provide a simple baseline against which we can measure more sophisticated feature extraction methods. Simply using the raw intensities as input vectors for the GNB is impractical, because the large size of the feature vector coupled with the weak correlation between any given pixel intensity and the image class leads to poor performance. This problem may be mitigated by reducing the dimension of the feature vector via principal component analysis (PCA). When PCA is performed on the pixel intensities and only the components necessary to account for 90\% of the variance are retained, the dimension of the feature vector drops from 3072 to approximately 100, a much more manageable number. When we trained a GNB using these feature vectors, the training error as determined by five-fold cross-validation was 64\% and the test error was 69\%. This is certainly not ideal performance, but it nonetheless provides a starting point.

A popular pipeline for feature extraction involves using the scale invariant feature transform (SIFT) to identify features in each image of the training set, then
 clustering these features using an algorithm such as k-means. These clusters are then used as the dictionary for a visual bag of words approach, in which each image is
 represented as a histogram of various visual features. This approach is successful with many images, and pairing this method of feature extraction with classifiers such
 as the support vector machine with a chi squared kernel allows for straightforward image classification. When applied to the portion of the CIFAR-10 dataset used for
 training, however, problems arise. The size and resolution of the images mean that even when its edge and peak thresholds are set very low, SIFT returns a few features at most for each image, which is not sufficient to reliably classify images. Clearly, we would prefer a method of feature extraction that provides more information for each image. 

There are two methods that are commonly used to provide a great deal of usable information even from very small images: dense SIFT and the histogram of oriented gradients (HoG). Dense SIFT produces feature descriptors of the same type as SIFT, but instead of only returning those descriptors for detected features, as SIFT does, dense SIFT generates those descriptors for a window that slides over the image, returning a large number of features. These features are reduced using PCA as above, and used to train a GNB, which produces a training error of 42\% and a test error of 54\%, a clear improvement over the raw intensities. HoG operates by dividing the image into sections and describing the gradient of the intensity in each section, and so similarly produces a large number of features. When the same training process is followed, it yields a training error of 45\% and a testing error of 52\%.



\section{Conclusion}

% \subsubsection*{Acknowledgments}

\subsubsection*{References}

% References follow the acknowledgments. Use unnumbered third level heading for
% the references. Any choice of citation style is acceptable as long as you are
% consistent. It is permissible to reduce the font size to `small' (9-point) 
% when listing the references. {\bf Remember that this year you can use
% a ninth page as long as it contains \emph{only} cited references.}

% \small{
% [1] Alexander, J.A. \& Mozer, M.C. (1995) Template-based algorithms
% for connectionist rule extraction. In G. Tesauro, D. S. Touretzky
% and T.K. Leen (eds.), {\it Advances in Neural Information Processing
% Systems 7}, pp. 609-616. Cambridge, MA: MIT Press.

% [2] Bower, J.M. \& Beeman, D. (1995) {\it The Book of GENESIS: Exploring
% Realistic Neural Models with the GEneral NEural SImulation System.}
% New York: TELOS/Springer-Verlag.

% [3] Hasselmo, M.E., Schnell, E. \& Barkai, E. (1995) Dynamics of learning
% and recall at excitatory recurrent synapses and cholinergic modulation
% in rat hippocampal region CA3. {\it Journal of Neuroscience}
% {\bf 15}(7):5249-5262.
% }

\end{document}
