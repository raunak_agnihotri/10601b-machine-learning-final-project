function y = classify(Model,XTest)
M = Model{1,1};
V = Model{1,2};
p = Model{1,3};

y = zeros(size(XTest,1),1);
for j = 1:size(XTest,1) % for every training example
    ys = zeros(length(p),1);
    for k = 1:length(p) % for each class
        logprod = 0;
        for i = 1:size(XTest,2) % for each feature
            logprod = logprod + log(normpdf(double(XTest(j,i)),double(M(i,k)),double(V(i,k)^0.5)));
        end
        ys(k) = log(p(k)) + logprod;
    end
    [~,argmax] = max(ys);
    y(j) = argmax; 
end
end