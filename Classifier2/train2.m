function Model = train(XTrain,YTrain)
%% Gaussian Naive Bayes Classifier
n = size(XTrain,1); % number of training examples
m = size(XTrain,2); % number of features
%% Set priors
prior = zeros(1,10);
for k=1:10
    prior(k) = sum(YTrain == k);
end
prior = prior/length(YTrain);

%% Estimate mean and variance
M = zeros(m,k); 
V = zeros(m,k);
for i=1:m
    for j=1:k
        M(i,j) = mean(XTrain(:,i)); % change this later, so that mu depends on both i and j
        V(i,j) = var(double(XTrain(:,i))); % change this later, so that mu depends on both i and j
    end
end

%% Save the model parameters
Model = {M,V,prior};
save('Model.mat', 'Model');
end
