function [lg] = logGaussianProb(value, mean, variance),
  lg = - (value - mean)^2 / (2 * variance) - log(2 * pi * variance) / 2;
endfunction;
