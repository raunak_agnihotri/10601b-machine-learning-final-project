function [train_data, train_labels]=load_train_data()
  train_data = [];
  train_labels = [];
  
  load('subset_CIFAR10/small_data_batch_1.mat');
  train_data = cat(dim=1, train_data, data);
  train_labels = cat(dim=1, train_labels, labels);
  
  load('subset_CIFAR10/small_data_batch_2.mat');
  train_data = cat(dim=1, train_data, data);
  train_labels = cat(dim=1, train_labels, labels);
  
  load('subset_CIFAR10/small_data_batch_3.mat');
  train_data = cat(dim=1, train_data, data);
  train_labels = cat(dim=1, train_labels, labels);
  
  load('subset_CIFAR10/small_data_batch_4.mat');
  train_data = cat(dim=1, train_data, data);
  train_labels = cat(dim=1, train_labels, labels);
  
  load('subset_CIFAR10/small_data_batch_5.mat');
  train_data = cat(dim=1, train_data, data);
  train_labels = cat(dim=1, train_labels, labels);
  
endfunction