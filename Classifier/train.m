vl_setup;
[train_data, train_labels] = load_train_data();

train_data_hog = cellfun(@hog_extract, num2cell(train_data, dim=2), \
		     'UniformOutput', false);
train_data_hog = horzcat(train_data_hog{:})';

pca_vecs_hog = pca(train_data_hog, variance_threshold=0.9);
train_data_hog_pca = train_data_hog * pca_vecs_hog;

p = prior(train_labels);
[M, V] = likelihood(train_data_hog_pca, train_labels);

Model = struct('pca_vecs', pca_vecs_hog, 'means', M, 'variances', V, \
	       'priors', p);
save('Model.mat', 'Model');