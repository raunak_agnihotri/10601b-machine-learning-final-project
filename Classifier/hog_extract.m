function [hog]=hog_extract(data_cell, cell_size=8)
  image = single(reshape(data_cell, 32, 32, 3));
  hog = vl_hog(image, cell_size);
  hog = hog(:);
endfunction
