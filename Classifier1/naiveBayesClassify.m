function [t] = naiveBayesClassify(xTest, M, V, p)  
  t = [];
  for x = xTest',
    y_pred = 0;
    y_pred_prob = -Inf;
    
    for y = 1:rows(p),
      gaussian_inputs = [x, M(:, y), V(:, y)]';
      log_gaussian_probs = zeros(1, rows(x));
      
      for index = 1:rows(x),
	input = gaussian_inputs(:, index);
	log_gaussian_probs(index) = logGaussianProb(input(1), input(2), input(3));
      endfor;
      
      log_unnormed_prob = logProd([log_gaussian_probs, log(p(y))]);
      if log_unnormed_prob > y_pred_prob,
	y_pred = y;
	y_pred_prob = log_unnormed_prob;
      endif;
      
    endfor;
    t(end+1) = y_pred;
  endfor;
  t = t';
endfunction;
