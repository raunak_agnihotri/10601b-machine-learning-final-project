function train1(train_data, train_labels)
  vl_setup;

  pca_var_thresh = 0.9;

  ## perform extraction of dense SIFT features
  train_data_dsift = cellfun(@dsift_extract, num2cell(train_data, dim=2), 'UniformOutput', false);
  train_data_dsift = vertcat(train_data_dsift{:});

  ## reduce dimensionality by PCA
  pca_vecs_dsift = pca(train_data_dsift, variance_threshold=pca_var_thresh);
  train_data_dsift_pca = train_data_dsift * pca_vecs_dsift;

  p = prior(train_labels);
  [M, V] = likelihood(train_data_dsift_pca, train_labels);

  Model1 = struct('pca_vecs', pca_vecs_dsift, 'means', M, 'variances', V, \
	       'priors', p);
  save('Model1.mat', 'Model1');

endfunction