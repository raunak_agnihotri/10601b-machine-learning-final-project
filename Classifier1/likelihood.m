function [M, V] = likelihood(xTrain, yTrain)
  for i = 1:rows(unique(yTrain)),
    M(:, i) = sum(xTrain .* (yTrain == i)) / sum(yTrain == i);
    V(:, i) = sum((xTrain - M(:, i)').**2 .* (yTrain == i)) / sum(yTrain == i);
  end;
end
