function train2(train_data, train_labels)
  pca_var_thresh = 0.9;

  ## reduce dimensionality by PCA
  pca_vecs_raw = pca(train_data, variance_threshold=pca_var_thresh);
  train_data_raw_pca = double(train_data) * pca_vecs_raw;

  p = prior(train_labels);
  [M, V] = likelihood(train_data_raw_pca, train_labels);

  Model2 = struct('pca_vecs', pca_vecs_raw, 'means', M, 'variances', V, \
	       'priors', p);
  save('Model2.mat', 'Model2');

endfunction