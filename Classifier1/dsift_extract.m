function [descriptors] = dsift_extract (data_cell, step=4, size=4)
  [frames, descriptors] = vl_dsift(single(rgb2gray(reshape(data_cell, \
							  32, 32, \
							  3))), \
				   'fast', 'floatdescriptors', 'step', \
				   step, 'size', size);
  descriptors = descriptors(:)';
endfunction
