function [y] = classify(Model, X)

test_data_hog = cellfun(@hog_extract, num2cell(X, dim=2), 'UniformOutput', false);
test_data_hog = horzcat(test_data_hog{:})';

## reduce dimensionality by PCA
test_data_hog_pca = test_data_hog * Model.pca_vecs;

y = naiveBayesClassify(test_data_hog_pca, Model.means, Model.variances, Model.priors);

endfunction