function [y] = classify1(Model, X)

test_data_dsift = cellfun(@dsift_extract, num2cell(X, dim=2), 'UniformOutput', false);
test_data_dsift = vertcat(test_data_dsift{:});

## reduce dimensionality by PCA
test_data_dsift_pca = test_data_dsift * Model.pca_vecs;

y = naiveBayesClassify(test_data_dsift_pca, Model.means, Model.variances, Model.priors);

endfunction