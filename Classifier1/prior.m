function [p] = prior(yTrain)
	 counts = histc(yTrain, unique(yTrain));
	 p = counts / sum(counts);
end
